import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc'

dayjs.extend(utc)

export type Quarter = {
	quarter: string,
	name: string,
	update_time: string,
	sessions: string[],
	buildings?: { [index: string]: Building }
}

export type Building = {
	building: string,
	name: string,
	rooms?: { [index: string]: Room }
}

export type Room = {
	room: string,
	name: string,
	meetings: { [index: string]: Meeting[] },
	items: { [index: string]: Item },
	max_size: number
}

export type Meeting = {
	day?: string,
	start: dayjs.Dayjs,
	end: dayjs.Dayjs,
	item?: string
}

export type Item = Class | Section

export type Class = {
	course: string,
	title: string,
	full_title: string,
	instructors: string[],
	enrolled: number,
	maximum: number,
	meetings: Meeting[],
	session?: string,
	is_section: boolean
}

export type Section = Class & {
	class_instructors: string[],
	class_meetings: Meeting[]
}

var quarters: { [index: string]: Quarter }

async function getQuarters() {
	if(!quarters) {
		const path = './api/quarters.json'

		quarters = await get(path)
	}
	return quarters
}

async function getQuarter(quarter: string) {
	await getQuarters()

	if(!quarters[quarter]) throw new Error(`Quarter ${quarter} does not exist`)

	if(!quarters[quarter].buildings) {
		const path = `./api/${quarter}/buildings.json`

		quarters[quarter].buildings = await get(path)
	}
	return quarters[quarter]
}

async function getBuilding(quarter: string, building: string, sessions?: string[]) {
	await getQuarter(quarter)

	if(!quarters[quarter].buildings[building]) throw new Error(`Building ${building} does not exist`)

	if(!quarters[quarter].buildings[building].rooms) {
		const path = `./api/${quarter}/${building}.json`

		type RoomsRes = {
			rooms: { [index: string]: Room }
		}
		const res: RoomsRes = await get(path)

		//convert time strings to day.js
		const toDayjs = (time) => dayjs.utc(time)

		for(const room of Object.values(res.rooms)) {
			for(const day of Object.values(room.meetings)) {
				for(const meeting of day) {
					meeting.start = toDayjs(meeting.start)
					meeting.end = toDayjs(meeting.end)
				}
			}
			for(const item of Object.values(room.items)) {
				for(const meeting of item.meetings) {
					meeting.start = toDayjs(meeting.start)
					meeting.end = toDayjs(meeting.end)
				}
			}
		}

		quarters[quarter].buildings[building].rooms = res.rooms
	}
	let res = quarters[quarter].buildings[building]
	if(sessions) {
		res = Object.assign({}, res)
		res.rooms = Object.fromEntries(Object.entries(res.rooms)
			.map(function(entry): [string, Room] {
				const room = Object.assign({}, entry[1])

				room.items = Object.fromEntries(Object.entries(room.items)
					.filter(entry => !entry[1].session || sessions.includes(entry[1].session))
				)
				room.meetings = Object.fromEntries(Object.entries(room.meetings)
					.map(entry => [entry[0], entry[1]
						.filter((meeting: Meeting) => room.items[meeting.item])
					])
				)

				return [entry[0], room]
			})
			.filter(entry => Object.values(entry[1].items).length > 0)
		)
	}
	return res
}

var waiting: { [index: string]: Promise<any> } = {}

async function get(path: string) {
	const realGet = async (path: string) => {
		const res = await fetch(path)

		if(res.ok) {
			return await res.json()
		} else {
			throw new Error(`Request to ${path} failed`)
		}
	}

	if(!waiting[path]) {
		waiting[path] = realGet(path)
	}
	const res = await waiting[path]
	waiting[path] = undefined
	return res
}

export default { getQuarters, getQuarter, getBuilding }