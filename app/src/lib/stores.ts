import { readable, writable } from 'svelte/store'
import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc'
import timeZone from 'dayjs/plugin/timeZone'
import customParseFormat from 'dayjs/plugin/customParseFormat'
import objectSupport from 'dayjs/plugin/objectSupport'

dayjs.extend(utc)
dayjs.extend(timeZone)
dayjs.extend(objectSupport)
dayjs.extend(customParseFormat)

const createTime = () => {
	const currentTime = () => dayjs().tz('America/Los_Angeles').startOf('minute')

	const getTime = (datetime: dayjs.Dayjs) => dayjs.utc(Object.assign({ 'year': 1900, 'month': 0, 'day': 1 }, { 'hour': datetime.hour(), 'minute': datetime.minute() }))
	const getDay = (datetime: dayjs.Dayjs) => ['Su', 'M', 'T', 'W', 'R', 'F', 'Sa'][datetime.day()]
	const generateForms = (time: dayjs.Dayjs) => {
		return {
			'datetime': time,
			'time': getTime(time),
			'day': getDay(time),
			'isCustom': false
		}
	}

	const initial = currentTime()
	const time = readable(initial, function start(set, update) {
		let value = initial

		const attemptTimeUpdate = () => {
			const newTime = currentTime()

			if(!value.isSame(newTime, 'minute')) {
				value = newTime
				set(newTime)
			}
		}

		const startInterval = () => setInterval(attemptTimeUpdate, 1000)
		let interval = startInterval()

		const visibilityCallback = () => {
			if(document.hidden) {
				clearInterval(interval)
			} else {
				attemptTimeUpdate()
				interval = startInterval()
			}
		}
		document.addEventListener('visibilitychange', visibilityCallback)

		return function stop() {
			document.removeEventListener('visibilitychange', visibilityCallback)
			clearInterval(interval)
		}
	})

	const { subscribe, set, update } = writable(generateForms(initial), function start(set, update) {
		const unsubscribe = time.subscribe(value => {
			update(current => {
				if(current.isCustom) {
					return current // could potentially be improved
				} else {
					return generateForms(value)
				}
			})
		})
		return function stop() {
			unsubscribe()
		}
	})

	const setCustom = (day: string, time:string) => {
		set({
			'datetime': null,
			'time': getTime(dayjs.utc(time, 'HH:mm')),
			'day': day,
			'isCustom': true
		})
	}
	const reset = () => {
		set(generateForms(currentTime()))
	}

	return { subscribe, setCustom, reset }
}

export const time = createTime()