import { defineConfig } from 'vite'
import { svelte } from '@sveltejs/vite-plugin-svelte'

// https://vitejs.dev/config/
export default defineConfig(({command, mode, ssrBuild }) => {
  if(command === 'build') {
    return {
      plugins: [svelte()],
      base: './'
    }
  }
  return {
    plugins: [svelte()],
  }
})
