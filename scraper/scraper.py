from datetime import datetime, timezone
from bs4 import BeautifulSoup
from requests_html import HTMLSession
from urllib.parse import urljoin
import json
import os

htmlsession = HTMLSession()

def get_forms(url):
	res = htmlsession.get(url)
	soup = BeautifulSoup(res.html.html, "html.parser")
	return soup.find_all("form")

def get_form_details(form):
	details = {}

	# URL
	action = form.attrs.get("action").lower()

	# POST, GET, etc
	method = form.attrs.get("method", "get").lower()

	inputs = []
	for input_tag in form.find_all("input"):
		input_type = input_tag.attrs.get("type","text")
		input_name = input_tag.attrs.get("name")
		input_value = input_tag.attrs.get("value","")
		inputs.append({
			"type": input_type,
			"name": input_name,
			"value": input_value
		})
	
	for select in form.find_all("select"):
		select_name = select.attrs.get("name")

		select_type = "select"

		select_options = []
		
		select_default_value = ""

		for select_option in select.find_all("option"):
			option_value = select_option.attrs.get("value")
			option_name = select_option.string

			option = {
				"value": option_value,
				"name": option_name
			}

			if option_value:
				select_options.append(option)
				if select_option.attrs.get("selected"):
					select_default_value = option
		if not select_default_value and select_options:
			select_default_value = select_options[0]["value"]
		
		inputs.append({
			"type": select_type,
			"name": select_name,
			"values": select_options,
			"value": select_default_value
		})
	
	for textarea in form.find_all("textarea"):
		textarea_name = textarea.attrs.get("name")
		textarea_type = "textarea"
		textarea_value = textarea.attrs.get("value","")
		inputs.append({
			"type": textarea_type,
			"name": textarea_name,
			"value": textarea_value
		})

	details["action"] = action
	details["method"] = method
	details["inputs"] = inputs
	return details

def fill_form(url, form_details, department, quarter, level):
	data = {
		"ctl00$pageContent1$courseList": department,
		"ctl00$pageContent1$quarterList": quarter,
		"ctl00$pageContent1$dropDownCourseLevels": level,
		"ctl00$pageContent1$searchButton.x": 0,
		"ctl00$pageContent1$searchButton.y": 1
	}

	for input in form_details["inputs"]:
		if input["type"] == "hidden": # use default for hidden
			data[input["name"]] = input["value"]

	url = urljoin(url, form_details["action"])
	htmlsession.headers["Content-Type"] = "application/x-www-form-urlencoded"
	
	print(f"Getting {department}, {quarter}, {level}")

	return htmlsession.post(url, data=data)

def extract_data(res):
	soup = BeautifulSoup(res.html.html, "html.parser")
	rows = soup.find_all("tr", "CourseInfoRow")

	courses = {}
	classes = {}
	sections = {}

	current_class = None
	latest_section = None


	for row in rows:
		data = parse_row(row)

		if data["has_section"]:
			# CLASS

			courses[data["course"]] = {
				"course": data["course"],
				"title": data["title"],
				"full_title": data["full_title"],
				"description": data["description"],
				"prerequisite": data["prerequisite"],
				"college": data["college"],
				"units": data["units"],
				"grading": data["grading"]
			}

			# register previous class
			if current_class != None:
				classes[current_class["class"]] = current_class
			
			# process most information for class
			current_class = {
				"class": data["enroll_code"],
				"course": data["course"],
				"instructors": data["instructors"],
				"enrolled": data["enrolled"],
				"maximum": data["maximum"],
				"meetings": [],
				"sections": [],

				"status": data["status"],
				"session": data["session"],

				"level_limit": data["level_limit"],
				"major_limit_pass": data["major_limit_pass"],
				"major_limit": data["major_limit"],
				"messages": data["messages"]
			}

			for day in data["days"]:
				meeting = {
					"day": day,
					"start": data["start"],
					"end": data["end"],
					"location": data["location"]
				}
				current_class["meetings"].append(meeting)
			
			latest_section = None
			
		else:
			if data["enroll_code"] == "":
				# ADDITIONAL MEETINGS

				# process additional meeting times
				for day in data["days"]:
					meeting = {
						"day": day,
						"start": data["start"],
						"end": data["end"],
						"location": data["location"]
					}
					if latest_section == None:
						current_class["meetings"].append(meeting)
					else:
						sections[latest_section]["meetings"].append(meeting)
					
			else:
				#SECTION

				# class with sections takes id of first section
				if current_class["class"] == "":
					current_class["class"] = data["enroll_code"]
					current_class["session"] = data["session"]

				# process section
				section = {
					"section": data["enroll_code"],
					"course": data["course"],
					"class": current_class["class"],
					"instructors": data["instructors"],
					"enrolled": data["enrolled"],
					"maximum": data["maximum"],
					"meetings": [],

					"status": data["status"],
					"session": data["session"]
				}
				for day in data["days"]:
					meeting = {
						"day": day,
						"start": data["start"],
						"end": data["end"],
						"location": data["location"]
					}
					section["meetings"].append(meeting)

				if data["status"] != "Cancelled":
					sections[section["section"]] = section
					current_class["sections"].append(section["section"])
				
				latest_section = section["section"]
	

	if current_class != None:
		classes[current_class["class"]] = current_class

	return {
		"courses": courses,
		"classes": classes,
		"sections": sections
	}

#
#

def parse_row(row):
	cells = row.find_all("td", recursive=False)

	course = td_text(cells[1])
	title = td_text(cells[2])
	status = td_text(cells[3])

	codes = td_text(cells[4]).split("/")
	enroll_code = codes[0]
	if len(codes) > 1:
		session = codes[1]
	else:
		session = None
	
	instructors = td_texts(cells[5])

	days = filter(lambda letter: letter in ['M','T','W','R','F'], td_text(cells[6]))

	times = td_text(cells[7]).split(" - ")
	if len(times) < 2:
		start = None
		end = None
	else:
		start = convert_time(times[0])
		end = convert_time(times[1])

	location = td_text(cells[8])
	enrolled = td_text(cells[9]).split(" / ")
	has_section = td_text(cells[10]) == "True"

	course_table_raw = cells[1].find("table","MasterCourseTable")
	course_table = read_table(course_table_raw)

	full_title = course_table["Full Title"]
	description = course_table["Description"]
	prerequisite = course_table["PreRequisite"]
	college = course_table["College"]
	units = course_table["Units"]
	grading = course_table["Grading"]

	restrictions_table_raw = cells[4].find("table", "RestrictionsTable")
	restrictions_table = read_table(restrictions_table_raw)

	level_limit = restrictions_table["Level-Limit"]
	major_limit_pass = restrictions_table["Major-Limit-Pass"]
	major_limit = restrictions_table["Major-Limit"]
	messages = restrictions_table["Messages"]

	data = {
		"course": course,
		"title": title,
		"status": status,
		"enroll_code": enroll_code,
		"session": session,
		"instructors": instructors,
		"days": days,
		"start": start,
		"end": end,
		"location": location,
		"enrolled": int(enrolled[0]),
		"maximum": int(enrolled[1]),
		"has_section": has_section,

		"full_title": full_title,
		"description": description,
		"prerequisite": prerequisite,
		"college": college,
		"units": units,
		"grading": grading,

		"level_limit": level_limit,
		"major_limit_pass": major_limit_pass,
		"major_limit": major_limit,
		"messages": messages
	}

	return data

#
#

def read_table(raw):
	table = {}
	for tr in raw.find_all("tr", recursive=False):
		c = tr.find_all("td", recursive=False)
		key = c[0].string.strip()[:-1] # remove colon
		val = td_text(c[-1]).strip()
		if key != "":
			table[key] = val
	return table

def td_text(td): # td's have either a plain string or one embedded in a direct child
	for item in td.children:
		text = item.string
		if text != None and text.strip() != "":
			return text.strip()
	return ""

def td_texts(td):
	res = []
	for item in td.children:
		text = item.string
		if text != None and text.strip() != "":
			res.append(text.strip())
	return res

def convert_time(string):
	if " " in string:
		format = "%I:%M %p"
	else:
		format = "%I:%M%p"
	return datetime.strptime(string, format)

#
#

def save_raw(path, courses, classes, sections):
	if(os.path.exists(path)):
		empty(path)
	else:
		os.mkdir(path)

	with open(path + "/courses.json", "w") as file:
		json.dump(prep(courses), file)
	with open(path + "/classes.json", "w") as file:
		json.dump(prep(classes), file)
	with open(path + "/sections.json", "w") as file:
		json.dump(prep(sections), file)

def save_api(path, courses, classes, sections):
	if(os.path.exists(path)):
		empty(path)
	else:
		os.mkdir(path)

	building_ref = {}

	buildings_text = open("./buildings.json").read()
	buildings = json.loads(buildings_text)

	for building in buildings:
		building_id = building["building"]

		building_rooms = get_rooms(building_id, courses, classes, sections)

		if len(building_rooms) > 0:
			building_ref[building_id] = building

			building_path = path + "/" + building_id

			with open(building_path + ".json", "w") as file:
				json.dump(prep({
					"rooms": building_rooms,
				}), file)
	
	with open(path + "/buildings.json", "w") as file:
		json.dump(prep(building_ref), file)

def get_rooms(building_id, courses, classes, sections):
	res = {}

	def try_item(item_id, item):
		for meeting in item["meetings"]:
			location = meeting["location"]

			if location[:len(building_id)] == building_id:
				#set up room if new
				if not location in res:
					new_room(location)
				
				# update max size
				if item["maximum"] > res[location]["max_size"]:
					res[location]["max_size"] = item["maximum"]
				
				# register meeting
				res[location]["meetings"][meeting["day"]].append({
					"start": meeting["start"],
					"end": meeting["end"],
					"item": item_id
				})

				# register item
				res[location]["items"][item_id] = item


	def new_room(location):
		room_name = location[len(building_id):].strip()

		res[location] = {
			"room": location,
			"name": room_name,
			"meetings": {
				"M": [],
				"T": [],
				"W": [],
				"R": [],
				"F": []
			},
			"items": {},
			"max_size": 0
		}

	for c in classes.values():
		class_id = c["class"]
		item_id = "class" + class_id

		course = courses[c["course"]]
		item = {
			"course": c["course"],
			"title": course["title"],
			"full_title": course["full_title"],
			"instructors": c["instructors"],
			"enrolled": c["enrolled"],
			"maximum": c["maximum"],
			"meetings": c["meetings"],
			"is_section": False
		}
		if c["session"] != None:
			item["session"] = c["session"]

		if c["status"] != "Cancelled":
			try_item(item_id, item)
	
	for section in sections.values():
		section_id = section["section"]
		item_id = "section" + section_id

		course = courses[section["course"]]
		c = classes[section["class"]]
		item = {
			"course": section["course"],
			"title": course["title"],
			"full_title": course["full_title"],
			"instructors": section["instructors"],
			"enrolled": section["enrolled"],
			"maximum": section["maximum"],
			"meetings": section["meetings"],
			"class_instructors": c["instructors"],
			"class_meetings": c["meetings"],
			"is_section": True
		}
		if section["session"] != None:
			item["session"] = section["session"]

		if section["status"] != "Cancelled" and classes[section["class"]]["status"] != "Cancelled":
			try_item(item_id, item)
	
	#sort by room id (effectively sorts by room number)
	res = dict(sorted(res.items(), key=lambda item: item[0]))

	#sort meetings in each room by start time
	for room in res:
		for day in res[room]["meetings"]:
			res[room]["meetings"][day] = sorted(res[room]["meetings"][day], key=lambda meeting: meeting["start"].replace(tzinfo=timezone.utc).timestamp())

	return res

#
#

def empty(path):
	for file in os.listdir(path):
		file_path = path + "/" + file
		if os.path.isfile(file_path):
			os.remove(file_path)
		else:
			empty(file_path)
			os.rmdir(file_path)

def prep(obj):
	if type(obj).__name__ == "dict":
		return prep_dict(obj)
	if type(obj).__name__ == "list":
		return prep_list(obj)
	if type(obj).__name__ == "datetime":
		return obj.isoformat()
	return obj

def prep_list(list):
	res = []

	for item in list:
		res.append(prep(item))
	
	return res

def prep_dict(dict):
	res = {}

	for key in dict:
		res[key] = prep(dict[key])
	
	return res

#
#

if __name__ == "__main__":
	url = "https://my.sa.ucsb.edu/Public/curriculum/coursesearch.aspx"

	forms = get_forms(url)
	form_details = get_form_details(forms[0])

	for field in form_details["inputs"]:
		if field["name"] == "ctl00$pageContent1$courseList":
			opts = field["values"]
		if field["name"] == "ctl00$pageContent1$quarterList":
			values = field["values"]

			for i in range(len(values)):
				value = values[i]["value"]
				name = values[i]["name"]
				print(f"[{i+1}] {value}: {name}")
	
	select = input(f"Select quarter (1-{len(values)})")
	quarter = values[int(select)-1]["value"]
	quarter_name = values[int(select)-1]["name"].strip().lower().capitalize()

	raw_path = f"../raw/{quarter}"
	api_path = f"../api/{quarter}"

	update_time = datetime.utcnow()



	courses = {}
	classes = {}
	sections = {}

	for opt in opts:
		res = fill_form(url, form_details, opt["value"], quarter, "All")
		data = extract_data(res)
		courses.update(data["courses"])
		classes.update(data["classes"])
		sections.update(data["sections"])


	save_raw(raw_path, courses, classes, sections)

	# get all sessions for this quarter
	all_sessions = set()
	for c in classes.values():
		if c["session"] != None:
			all_sessions.add(c["session"])
	for section in sections.values():
		if section["session"] != None:
			all_sessions.add(section["session"])
	all_sessions = list(all_sessions)

	# update quarters.json
	quarters_read = open("../api/quarters.json")
	quarters = json.loads(quarters_read.read())
	quarters[quarter] = {
		"quarter": quarter,
		"name": quarter_name,
		"update_time": update_time,
		"sessions": all_sessions
	}
	quarters_write = open("../api/quarters.json", "w")
	json.dump(prep(quarters), quarters_write)

	save_api(api_path, courses, classes, sections)
